class Shoe {
	constructor(...decks) {
		this._cards = decks.reduce((acc, curr) =>
			([...acc, ...curr.getCards()])
		, []);
	}

	drawCard() {
		return this._cards.pop();
	}

	getCardsLeft() {
		return this._cards.length;
	}
}