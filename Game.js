class Game {
	constructor(numberOfDecks) {
		this._gameOver = false;

		const decks = [];
		for (let i = 0; i < numberOfDecks; i ++) {
			decks.push(new Deck());
		}

		this._shoe = new Shoe(...decks);
		this._dealer = new Player('Dealer', 1000);
		this._player = new Player('LapSiLap', 1000);
	}

	start() {
		this._player.deal(this._shoe.drawCard());
		this._dealer.deal(this._shoe.drawCard());
		this._player.deal(this._shoe.drawCard());
	}

	newRound() {
		if (!this._gameOver) return;

		this._player.clearCards();
		this._dealer.clearCards();
		this._gameOver = false;
		this.start();
	}

	hit() {
		if (this._gameOver) return;

		this._player.deal(this._shoe.drawCard());

		if (this._player.isBusted()) {
			this.endGame();
		}
	}

	stand() {
		if (this._gameOver) return;

		while (this._dealer.getScore() <= 17) {
			this._dealer.deal(this._shoe.drawCard());	
		}

		this.endGame();
	}

	endGame() {
		this._gameOver = true;

		if (this._getWinner() === this._player) {
			if (this._player.hasBlackJack()) {
				this._player.giveCash(150);
				this._dealer.takeCash(150);
			} else {
				this._player.giveCash(100);
				this._dealer.takeCash(100);
			}
		} else {
			this._dealer.giveCash(100);
			this._player.takeCash(100);
		}
	}

	_getWinner() {
		if (this._player.hasBlackJack() && !this._dealer.hasBlackJack()) {
			return this._player;
		}

		if (this._player.hasBlackJack() && this._dealer.hasBlackJack()) {
			return null;
		}

		if (this._player.hasBlackJack()) {
			return this._player;
		}

		if (this._dealer.hasBlackJack()) {
			return this._dealer;
		}

		if (this._player.isBusted()) {
			return this._dealer;
		}

		if (this._dealer.isBusted()) {
			return this._player;
		}

		if(this._player.getScore() > this._dealer.getScore()) {
			return this._player;
		}

		if (this._dealer.getScore() > this._player.getScore()) {
			return this._dealer;
		}

		// Draw
		return null;
	}

	printGame() {
		let text = '';

		text += `<b>Cards left</b>: ${this._shoe.getCardsLeft()}<br /><br />`; 

		text += this._dealer.getState();
		text += '<br /><br />';
		text += this._player.getState();

		if (this._gameOver) {
			text += '<br /><br />';
			const winner = this._getWinner();

			if (winner === this._player) {
				text += '<h2>You won</h2>'
			} else if (winner === this._dealer) {
				text += '<h2>You lost</h2>';
			} else {
				text += '<h2>Draw</h2>';
			}

		}

		return text;
	}
}
