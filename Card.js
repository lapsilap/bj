const map = {
	1: 'Ace',
	2: 'Two',
	3: 'Three',
	4: 'Four',
	5: 'Five',
	6: 'Six',
	7: 'Seven',
	8: 'Eight',
	9: 'Nine',
	10: 'Ten',
	11: 'Jack',
	12: 'Queen',
	13: 'King',
};

class Card {
	static HEARTS = 0;
	static SPADES = 1;
	static CLUBS = 2;
	static DIAMONDS = 3;

	constructor(value, suit) {
		this._value = value;
		this._suit = suit;
	}

	getValue() {
		if (this._value === 1) {
			return 11;
		}

		if (this._value >= 10) {
			return 10;
		}

		return this._value;
	}

	getFriendlyValue() {
		switch(this._suit) {
			case Card.HEARTS:
				return `${map[this._value]} of Hearts`;
			case Card.SPADES:
				return `${map[this._value]} of Spade`;
			case Card.CLUBS:
				return `${map[this._value]} of Clubs`;
			case Card.DIAMONDS:
				return `${map[this._value]} of Diamonds`;
		}
	}
}