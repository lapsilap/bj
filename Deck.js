class Deck {
	constructor() {
		this._cards = [];

		for (let value = 1; value <= 13; value++) {
			this._cards.push(new Card(value, Card.HEARTS));
			this._cards.push(new Card(value, Card.DIAMONDS));
			this._cards.push(new Card(value, Card.CLUBS));
			this._cards.push(new Card(value, Card.SPADES));
		}

		this._shuffle();
	}

	getCards() {
		return this._cards;
	}

	_shuffle() {
    	let j, x, i;

	    for (i = this._cards.length - 1; i > 0; i--) {
	        j = Math.floor(Math.random() * (i + 1));
	        x = this._cards[i];
	        this._cards[i] = this._cards[j];
	        this._cards[j] = x;
	    }
    }
}