let game = null;

function newGame() {
	game = new Game(parseInt(document.getElementById('number-of-decks').value));
	game.start();
}

function update() {
	document
		.getElementById('game')
		.innerHTML = game.printGame()
}

document
	.getElementById('new-game')
	.addEventListener('click', () => {
		newGame();
		update();
	});

document
	.getElementById('hit')
	.addEventListener('click', () => {
		game.hit();
		update();
	});

document
	.getElementById('stand')
	.addEventListener('click', () => {
		game.stand();
		update();
	});

document
	.getElementById('new-round')
	.addEventListener('click', () => {
		game.newRound();
		update();
	});
