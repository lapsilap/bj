class Player {
	constructor(name, cash) {
		this._name = name;
		this._cards = [];
		this._cash = cash;
	}

	deal(card) {
		this._cards.push(card);
	}

	clearCards() {
		this._cards = [];
	}

	_hasAce() {
		return this._cards.some(card => card.getValue() === 1);
	}

	getCards() {
		return this._cards;
	}

	getScore() {
		const score = this._cards.reduce(
			(score, card) =>
				score + card.getValue(),
			0
		);

		return this._hasAce() && score >= 22 ? score - 10 : score;
	}

	isBusted() {
		return this.getScore() > 21;
	}

	takeCash(cash) {
		this._cash -= cash;
	}

	giveCash(cash) {
		this._cash += cash;
	}

	hasBlackJack() {
		if (this.getCards().length === 2) {
			const [a, b] = this.getCards();
			if (a.getValue() === 11 && b.getValue() === 10) {
				return true;
			} else if (b.getValue() === 11 && a.getValue() === 10) {
				return true;
			}
		}

		return false;
	}

	getState() {
		let text = '';

		text += `<b>${this._name} (Score: ${this.getScore()}) (Cash: $${this._cash})</b><br />`;

		if (this.hasBlackJack()) {
			text += 'BLACKJACK!<br />';
		}

		text += this
			.getCards()
			.map(card => card.getFriendlyValue())
			.join('<br />');

		return text;
	}
}